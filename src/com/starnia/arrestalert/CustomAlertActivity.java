package com.starnia.arrestalert;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Matt on 12/19/13.
 */
public class CustomAlertActivity extends FragmentActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_alert_activity);

        CustomAlertFragment fragment;

        if (savedInstanceState == null) {
            fragment = new CustomAlertFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.notification_container, fragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_notification_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.add_notification_menu:
                startActivity(new Intent(getBaseContext(),
                        AddNewCustomNotificationActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
