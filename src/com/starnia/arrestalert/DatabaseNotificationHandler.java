package com.starnia.arrestalert;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Matt on 12/9/13.
 */
public class DatabaseNotificationHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ArrestAlert";

    public DatabaseNotificationHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS arrest");
        db.execSQL("DROP TABLE IF EXISTS notification");

        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // String name, charges, id, book_date_formatted, mugshot, book_date, more_info_url;
        String CREATE_ARREST_TABLE = "CREATE TABLE arrest (id INTEGER PRIMARY KEY, " +
                                     "name TEXT, charges TEXT, arrest_id TEXT, book_date_formatted STRING, " +
                                     "mugshot STRING, book_date STRING, more_info_url STRING, details STRING)";
        db.execSQL(CREATE_ARREST_TABLE);

        String CREATE_NOTIFICATION_TABLE = "CREATE TABLE notification (id INTEGER PRIMARY KEY, " +
                "last_name TEXT, first_name TEXT, state TEXT, county TEXT, source_id TEXT)";
        db.execSQL(CREATE_NOTIFICATION_TABLE);
    }

    public ArrayList<Arrest> getStoredArrests() {
        ArrayList<Arrest> storedArrests = new ArrayList<Arrest>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM arrest", null);

        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(1);
                String charges = cursor.getString(2);
                String arrestId = cursor.getString(3);
                String bookDateFormatted = cursor.getString(4);
                String mugshot = cursor.getString(5);
                String bookDate = cursor.getString(6);
                String moreInfoUrl = cursor.getString(7);
                String details = cursor.getString(8);

                JSONArray detailsArray = null;
                HashMap<String, String> detailsMap = new HashMap<String, String>();
                try {
                    detailsArray = new JSONArray(details);
                    for (int i=0; i<detailsArray.length(); i++)
                    {
                        JSONArray tempArrestArray = detailsArray.getJSONArray(i);
                        detailsMap.put(tempArrestArray.getString(0), tempArrestArray.getString(1));
                    }
                } catch(JSONException e) {
                    Log.e("JSON EXCEPTION: ", e.toString());
                } finally {
                    Arrest arrest = new Arrest(name, charges, arrestId, bookDateFormatted, mugshot, bookDate,
                            moreInfoUrl, detailsMap);
                    storedArrests.add(arrest);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return storedArrests;
    }

    public void clearStoredArrests() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("arrest", null, null);
        db.close();
    }

    public void addStoredArrests(ArrayList<Arrest> arrests) {
        SQLiteDatabase db = this.getReadableDatabase();
        for (int i=0; i<arrests.size(); i++) {
            Arrest arrest = arrests.get(i);
            ContentValues values = new ContentValues();
            values.put("name", arrest.getName());
            values.put("charges", arrest.getCharges());
            values.put("arrest_id", arrest.getId());
            values.put("book_date_formatted", arrest.getBookDateFormatted());
            values.put("mugshot", arrest.getMugshot());
            values.put("book_date", arrest.getBook_date());
            values.put("more_info_url", arrest.getMoreInfoURL());
            values.put("details", arrest.getDetailsString());  // flatten the details string into a JSON array
            db.insert("arrest", null, values);
        }
        db.close();
    }

    public ArrayList<CustomNotification> getCustomAlerts() {
        ArrayList<CustomNotification> storedNotifications = new ArrayList<CustomNotification>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM notification", null);

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String firstName = cursor.getString(2);
                String lastName = cursor.getString(1);
                String state = cursor.getString(3);
                String county = cursor.getString(4);
                String sourceId = cursor.getString(5);

                CustomNotification notification = new CustomNotification(id, firstName, lastName, state, county, sourceId);
                storedNotifications.add(notification);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return storedNotifications;
    }

    public void addCustomNotification(String first, String last, String state, String county, String sourceId) {
        SQLiteDatabase db = this.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put("first_name", first);
            values.put("last_name", last);
            values.put("state", state);
            values.put("county", county);
            values.put("source_id", sourceId);
            db.insert("notification", null, values);
        db.close();
    }

    public void deleteCustomNotification(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("notification", "id=" + id, null);
        db.close();
    }
}
