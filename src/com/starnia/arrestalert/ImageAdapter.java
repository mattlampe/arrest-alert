package com.starnia.arrestalert;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Matt on 12/16/13.
 */
public class ImageAdapter extends BaseAdapter {

    private ImageLoader imageLoader;
    private static LayoutInflater inflater = null;
    private Context activity;

    String[] mugshots, descriptions;

    public ImageAdapter(Context activity, String[] mugshots, String[] descriptions) {
        super();
        this.activity = activity;
        this.mugshots = mugshots;
        this.descriptions = descriptions;
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getCount() {
        return mugshots.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.item, null);

        TextView text=(TextView)vi.findViewById(R.id.text);
        ImageView image=(ImageView)vi.findViewById(R.id.image);
        text.setText(descriptions[position]);
        imageLoader.displayImage(mugshots[position], image);
        return vi;
    }
}
