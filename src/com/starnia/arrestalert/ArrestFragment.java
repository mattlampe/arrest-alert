package com.starnia.arrestalert;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.*;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.MemoryCacheUtil;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayInputStream;
import java.util.List;

public class ArrestFragment extends Fragment {

	ImageView imageView;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.arrest_view, container, false);

        // { arrest.getBook_date(), arrest.getBookDateFormatted(), arrest.getCharges(), arrest.getMugshot() }
		String[] details = getArguments().getStringArray("com.example.arrestalert.ArrestDetails");

        final String bookDate = details[1];
        final String mugshot = details[3];
        final String name = details[4];
        final String charges = details[2];
        final String moreInfo = details[5];

        imageView = (ImageView)view.findViewById(R.id.arrest_image);
        List<Bitmap> cachedImagesList = MemoryCacheUtil.findCachedBitmapsForImageUri(mugshot, ImageLoader.getInstance().getMemoryCache());
        if (!cachedImagesList.isEmpty())
            imageView.setImageBitmap(cachedImagesList.get(0));
        else {

        AsyncHttpClient httpClient = new AsyncHttpClient();
        httpClient.get(mugshot, new BinaryHttpResponseHandler(new String[] {"image/gif", "image/jpeg"}) {
            @Override
            public void onSuccess(byte[] imageData) {
                setImage(imageData);
            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] binaryData, java.lang.Throwable error) {
                error.printStackTrace();
            }
        });
        }

        TextView nameView = (TextView)view.findViewById(R.id.name);
        nameView.setText(name);

        TextView bookDateView = (TextView)view.findViewById(R.id.book_date);
        bookDateView.setText("Book Date: " + bookDate);

        TextView chargeView= (TextView)view.findViewById(R.id.charges);
        try {
            String charge = "Charges: ";
            JSONArray jsonDetailsArray = new JSONArray(charges);
            for (int i=0; i<jsonDetailsArray.length(); i++) {
                charge += jsonDetailsArray.getString(i);
                if (i < jsonDetailsArray.length() - 1)
                    charge += "\n";
            }
            chargeView.setText(charge);
        } catch (JSONException e) {
            //TODO: catch JSONException
        }

        if (moreInfo != null) {
            TextView link = (TextView)view.findViewById(R.id.link);
            link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri uriUrl = Uri.parse(moreInfo);
                    Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                    startActivity(launchBrowser);
                }
            });
        }

        return view;
	}

    private void setImage(byte[] imageData) {
        Drawable drawable = Drawable.createFromStream(new ByteArrayInputStream(imageData), "src");
        imageView = (ImageView)getView().findViewById(R.id.arrest_image);
        imageView.setImageDrawable(drawable);
    }

    public void getMoreInfo(TextView v) {
        // { arrest.getBook_date(), arrest.getBookDateFormatted(), arrest.getCharges(), arrest.getMugshot() }
        String[] details = getArguments().getStringArray(
                "com.example.arrestalert.ArrestDetails");
        Uri uriUrl = Uri.parse(details[5]);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
