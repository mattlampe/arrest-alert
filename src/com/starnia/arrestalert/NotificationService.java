/*
 * This is an example I found on the internet.  I haven't had a chance to mess with it yet.
 */

package com.starnia.arrestalert;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.StringTokenizer;

public class NotificationService extends Service {

    private PowerManager.WakeLock mWakeLock;
    static ArrayList<String> fbFriendNames;
    static ArrayList<Arrest> arrestedFriends;
    static ArrayList<Arrest> arrestList, customArrestList;
    static ArrestListResponseHandler arrestListResponseHandler, customArrestListResponseHandler;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        arrestedFriends = new ArrayList<Arrest>();
    }

    private void handleIntent(Intent intent) {
        // obtain the wake lock
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "com.starnia.arrestalert.notification");
        mWakeLock.setReferenceCounted(false);
        if (mWakeLock != null && !mWakeLock.isHeld())
            mWakeLock.acquire();

        // check the global background data setting
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (cm == null)
        	stopSelf();  // could not acquire ConnectivityManager
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnected()) {
            stopSelf();
        }

        MyApp.clearMatchedArrests();
        getRecentArrests(1);
    }

    private void getFBFriends() {
        Session session = Session.getActiveSession();
        if (session == null) {
            session = Session.openActiveSessionFromCache(this);
        }
        if (session.isOpened()) {
            fbFriendNames = new ArrayList<String>();
            Request request = new Request(session, "me/friends", null, HttpMethod.GET, new Request.Callback() {
                @Override
                public void onCompleted(Response response) {

                    GraphObject go = response.getGraphObject();
                    JSONObject jso = go.getInnerJSONObject();
                    JSONArray arr;
                    try {
                        arr = jso.getJSONArray("data");
                        for (int i = 0; i < (arr.length()); i++) {
                            JSONObject json_obj = arr.getJSONObject(i);
                            String name = json_obj.getString("name");
                            fbFriendNames.add(name);
                            fbFriendNames.add("Marcus Harris");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        // populates the matched friends list from FB and jailbase
                        crossCheckFacebook();
                    }
                }
            });
            RequestAsyncTask task = new RequestAsyncTask(request);
            task.execute();
        } else {
            Log.e("FB_LOGIN_ERROR", "Failed to open FB session!");
            checkCustomNotifications();
        }
    }

    private ArrayList<CustomNotification> getCustomNotifications() {
        DatabaseNotificationHandler db = new DatabaseNotificationHandler(this.getApplicationContext());
        ArrayList<CustomNotification> customNotifications = db.getCustomAlerts();
        return customNotifications;
    }

    /**
     * This is called on 2.0+ (API level 5 or higher). Returning
     * START_NOT_STICKY tells the system to not restart the service if it is
     * killed because of poor resource (memory/cpu) conditions.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleIntent(intent);
        return START_NOT_STICKY;
    }

    /**
     * In onDestroy() we release our wake lock. This ensures that whenever the
     * Service stops (killed for resources, stopSelf() called, etc.), the wake
     * lock will be released.
     */
    public void onDestroy() {
        super.onDestroy();
        mWakeLock.release();
    }

    private void crossCheckFacebook() {
        for (int i = 0; i < MyApp.getArrestList().size(); i++) {
            Arrest arrest = MyApp.getArrestList().get(i);

            String name = MyApp.getArrestList().get(i).getName();
            StringTokenizer token = new StringTokenizer(name);

            String firstName = null;
            if (token.hasMoreTokens())
                firstName = token.nextToken();
            String lastName = null;
            while (token.hasMoreTokens())
                lastName = token.nextToken();

            // check to see if any facebook friends have been arrested
            for (String fbFriend : fbFriendNames) {
                fbFriend = fbFriend.toLowerCase(Locale.US);
                if (firstName != null && lastName != null &&
                        fbFriend.contains(firstName.toLowerCase()) &&
                        fbFriend.contains(lastName.toLowerCase())) {
                    if (!arrestedFriends.contains(arrest))
                        arrestedFriends.add(arrest);
                    break;
                }
            }
        }
        checkCustomNotifications();
    }

    public void getRecentArrests(final int start_page) {
        arrestList = new ArrayList<Arrest>();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        String source_id = prefs.getString("source_id", null);

        if (source_id == null) {
            stopSelf();
        }

        arrestListResponseHandler = new ArrestListResponseHandler(this.getApplicationContext(),
                arrestList, source_id, ArrestListResponseHandler.RECENT) {
            @Override
            public void load() {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NotificationService.this);
                boolean fbIntegration = prefs.getBoolean("background_service", false);
                if (fbIntegration) {
                    getFBFriends();
                } else {
                    checkCustomNotifications();
                }
            }

            @Override
            public void handleFailure(Throwable e) {
                stopSelf();
            }
        };

        arrestListResponseHandler.getRecentArrests(start_page);
    }

    public void showAlerts() {
        // if there are matched friends and both FB and custom notifications are done loading alert the user
        if (!arrestedFriends.isEmpty()) {
            DatabaseNotificationHandler dbHandler = new DatabaseNotificationHandler(this.getApplicationContext());
            // get a list of arrests that have already had notifications (so we don't repeat the same notification).
            ArrayList<Arrest> storedArrests = dbHandler.getStoredArrests();

            ArrayList<Arrest> alertFriends = new ArrayList<Arrest>();
            for (int i = 0; i < arrestedFriends.size(); i++) {
                Arrest friendArrest = arrestedFriends.get(i);
                boolean found = false;
                for (int j = 0; j < storedArrests.size(); j++) {
                    if (friendArrest.getName().equalsIgnoreCase(storedArrests.get(j).getName()) &&
                            friendArrest.getBook_date().equalsIgnoreCase(storedArrests.get(j).getBook_date())) {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    alertFriends.add(arrestedFriends.get(i));
            }
            // add ALL the newly found matched friend arrests
            //dbHandler.addStoredArrests(alertFriends);
            //dbHandler.close();

            // add the notification if there are arrests that have not been alerted yet
            if (!alertFriends.isEmpty()) {
                // set the global matched arrests list for viewing if user clicks on notification
                MyApp.setMatchedArrests(alertFriends);

                // handle your data
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this.getApplicationContext())
                        .setSmallIcon(R.drawable.icon)
                        .setContentTitle("Friends arrested!")
                        .setContentText("Click to see who's been arrested!")
                        .setAutoCancel(true)
                        .setVibrate(new long[]{0, 500, 250, 500})
                        .setLights(0xFFFFFF00, 500, 500);

                // create an intent for when user clicks on notification
                Intent resultIntent = new Intent(this, MatchedArrestsActivity.class);

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this.getApplicationContext());

                stackBuilder.addParentStack(MainActivity.class);
                stackBuilder.addParentStack(MatchedArrestsActivity.class);

                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                Notification arrestNotification = mBuilder.build();

                mNotificationManager.notify(1, arrestNotification);
            }
        }
        stopSelf();
    }

    public void checkCustomNotifications() {
        ArrayList<CustomNotification> customNotifications = getCustomNotifications();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String source_id = prefs.getString("source_id", null);

        if (source_id != null) {
            // check the custom notifications to see if any have been arrested
            final SearchPoolManager poolManager = new SearchPoolManager(this.getApplicationContext(), customNotifications.size()) {
                @Override
                public void handleCallback() {
                    showAlerts();
                }
            };
            for (CustomNotification cn : customNotifications) {
                String customFirst = cn.getFirstName();
                String customLast = cn.getLastName();
                String customSourceId = cn.getSourceId();
                final ArrayList<Arrest> customNotificationList = new ArrayList<Arrest>();

                ArrestListResponseHandler searchHandler = new ArrestListResponseHandler(this.getApplicationContext(),
                        customNotificationList, customSourceId, ArrestListResponseHandler.SEARCH) {
                    @Override
                    public void load() {
                        arrestedFriends.addAll(customNotificationList);
                        poolManager.markComplete(this);
                    }

                    @Override
                    public void handleFailure(Throwable e) {
                        stopSelf();  // just exit
                    }
                };
                poolManager.addSearchHandler(searchHandler);
                searchHandler.getSearchArrests(1, customLast, customFirst);
            }
        }
    }
}