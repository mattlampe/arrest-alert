package com.starnia.arrestalert;

import android.app.ProgressDialog;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mlampo on 11/13/13.
 */
public abstract class LocationHandler extends JsonHttpResponseHandler {

    private ArrayList<ArrestLocation> locations;
    private AsyncHttpClient client;

    protected LocationHandler() {
        client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(0, 10000);
    }

    public LocationHandler(ArrayList<ArrestLocation> locations) {
        client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(0, 10000);
        this.locations = locations;
    }

    public void getLocations() {
        client.get("http://www.jailbase.com/api/1/sources", this);
    }

    @Override
    public void onSuccess(JSONObject response) {
        JSONArray locationArray = null;
        try {
            if (response.getInt("status") == 1) {

                locationArray = response.getJSONArray("records");

                for (int count = 0; count < locationArray
                        .length(); count++) {

                    String state = locationArray
                            .getJSONObject(count).getString(
                                    "state");

                    String state_full = locationArray
                            .getJSONObject(count).getString(
                                    "state_full");

                    String county_name = locationArray
                            .getJSONObject(count).getString(
                                    "name");

                    boolean has_mugshots = locationArray
                            .getJSONObject(count).getBoolean(
                                    "has_mugshots");

                    String source_id = locationArray
                            .getJSONObject(count).getString(
                                    "source_id");

                    locations.add(new ArrestLocation(source_id, state,
                            county_name, state_full, has_mugshots));
                }
                load();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure(error);
    }

    public abstract void load();

    public abstract void handleFailure(Throwable e);
}
