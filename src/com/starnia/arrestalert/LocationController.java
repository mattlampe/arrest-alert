package com.starnia.arrestalert;

import com.loopj.android.http.AsyncHttpClient;

import java.util.ArrayList;

/**
 * Created by mlampo on 11/5/13.
 */
public class LocationController {

    private static LocationController locationController = null;

    // private constructor for singleton
    private LocationController(){}

    public static LocationController getInstance() {
        if (locationController == null)
            locationController = new LocationController();

        return locationController;
    }


}
