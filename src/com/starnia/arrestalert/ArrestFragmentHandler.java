package com.starnia.arrestalert;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by mlampo on 11/10/13.
 */
public abstract class ArrestFragmentHandler extends JsonHttpResponseHandler {

    protected AsyncHttpClient client;
    private String source_id;
    private Context context;

    protected ArrestFragmentHandler() {}

    public ArrestFragmentHandler(Context context, String source_id) {
        this.context = context;
        this.source_id = source_id;
        client = new AsyncHttpClient(); // used to pull Jailbase api info asynchronously
        client.setMaxRetriesAndTimeout(0, 10000);
    }

    public void getRecentArrests(final int start_page) {
        if (source_id == null) {
        	handleFailure(new Throwable("Invalid source_id"));
        }

        try {
            client.get("http://www.jailbase.com/api/1/recent/?source_id="
                    + URLEncoder.encode(source_id, "UTF-8") + "&page=" + URLEncoder.encode(Integer.toString(start_page), "UTF-8" ),
                    this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void getSearchArrests(final int start_page, String lastName, String firstName) {
        if (source_id == null) {
            handleFailure(new Throwable("Invalid source_id"));
        }

        try {
            client.get("http://www.jailbase.com/api/1/search/?source_id="
                    + URLEncoder.encode(source_id, "UTF-8") + "&last_name=" + lastName + "&first_name="
                    + firstName + "&page=" + URLEncoder.encode(Integer.toString(start_page), "UTF-8" ),
                    this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public abstract void load();

    public abstract void handleFailure(Throwable e);

}
