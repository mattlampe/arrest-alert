package com.starnia.arrestalert;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by mlampo on 11/10/13.
 */
public class MatchedArrestsFragment extends Fragment {

    private GridView arrestListView;
    static ImageAdapter arrayAdapter;
    static String[] mugshots, descriptions;
    static ArrayList<Arrest> arrestList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mugshots = new String[] {};  // mugshot url for each arrest
        descriptions = new String[] {};  // description for each arrest
        arrayAdapter = new ImageAdapter(getActivity(), mugshots, descriptions);  // lazy adapter for mugshot list
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view;

        view = inflater.inflate(R.layout.recent_arrests_list_view,
                container, false);
        if (view != null) {
            arrestListView = (GridView) view.findViewById(R.id.arrest_list);
            arrestList = MyApp.getMatchedArrests();
            mugshots = new String[arrestList.size()];
            descriptions = new String[arrestList.size()];
            for (int i = 0; i < mugshots.length; i++) {
                mugshots[i] = arrestList.get(i).getMugshot();
                descriptions[i] = arrestList.get(i).getName();
            }

            arrayAdapter = new ImageAdapter(getActivity(), mugshots, descriptions);
            arrestListView.setAdapter(arrayAdapter);
            arrayAdapter.notifyDataSetChanged();

            arrestListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    Intent i = new Intent(getActivity(), ArrestActivity.class);
                    Arrest arrest = arrestList.get(position);
                    String[] details = { arrest.getBook_date(), arrest.getBookDateFormatted(),
                            arrest.getCharges(), arrest.getMugshot(), arrest.getName(), arrest.getMoreInfoURL() };
                    i.putExtra("com.example.arrestalert.ArrestDetails", details);
                    startActivity(i);
                }
            });
        }
        return view;
    }
}
