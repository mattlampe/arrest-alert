package com.starnia.arrestalert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import org.acra.*;
import org.acra.annotation.*;

@ReportsCrashes(formKey = "", formUri = "http://107.22.81.240/error.php")
public class MyApp extends Application {

    public static HashMap<String, ArrestLocation> locations = new HashMap<String, ArrestLocation>();
	private static ArrayList<Arrest> arrestList = new ArrayList<Arrest>();
    private static ArrayList<Arrest> matchedArrests = new ArrayList<Arrest>();
	private static boolean settingsChanged;
    private static boolean loaded = false;

	public void onCreate() {
		super.onCreate();
        ACRA.init(this);
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .displayer(new FadeInBitmapDisplayer(500))
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();

		ImageLoader.getInstance().init(config);

        arrestList = new ArrayList<Arrest>();
		setSettingsChanged(false);
	}
		
	public static Collection<ArrestLocation> getLocations() {
		return locations.values();
	}

    public static HashMap<String, ArrestLocation> getLocationsMap() {
        return locations;
    }

    public static void setArrestList(ArrayList<Arrest> arrestList) {
        MyApp.arrestList.clear();
        for (Arrest arrest : arrestList) {
            MyApp.arrestList.add((Arrest)arrest.clone());
        }
    }

    public static void setLocations(ArrayList<ArrestLocation> locations) {
        MyApp.locations.clear();
        for (ArrestLocation location : locations) {
            MyApp.addLocation(location);
        }
    }

    public static ArrayList<Arrest> getArrestList() {
        return MyApp.arrestList;
    }

    public static ArrestLocation getLocationById(String source_id) {
        ArrestLocation location = null;
        if (locations.get(source_id) != null)
                location = (ArrestLocation)locations.get(source_id).clone();
        return location;
    }

	public static boolean isSettingsChanged() {
		return settingsChanged;
	}

    public static boolean isLoaded() {
        return loaded;
    }

    public static void setLoaded(boolean value) {
        loaded = value;
    }

	public static void setSettingsChanged(boolean settingsChanged) {
		MyApp.settingsChanged = settingsChanged;
	}

    public static void addLocation(String source_id, String state, String county_name,
                                   String state_full, boolean has_mugshots) {
        if (locations != null) {
            locations.put(source_id, new ArrestLocation(source_id, state, county_name, state_full, has_mugshots));
        }
    }

    public static void addLocation(ArrestLocation location) {
        if (locations != null) {
            locations.put(location.getSourceId(), location);
        }
    }

    public static ArrayList<Arrest> getMatchedArrests() {
        return MyApp.matchedArrests;
    }

    public static void setMatchedArrests(ArrayList<Arrest> arrests) {
        MyApp.matchedArrests.clear();
        MyApp.matchedArrests.addAll(arrests);
    }

    public static void addMatchedArrests(ArrayList<Arrest> arrests) {
        MyApp.matchedArrests.addAll(arrests);
    }

    public static void clearMatchedArrests() {
        MyApp.matchedArrests.clear();
    }

    public static void clearArrestList() {
        MyApp.arrestList.clear();
    }
}
