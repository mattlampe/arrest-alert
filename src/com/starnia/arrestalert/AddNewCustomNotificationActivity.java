package com.starnia.arrestalert;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

/**
 * Created by Matt on 12/22/13.
 */
public class AddNewCustomNotificationActivity extends FragmentActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_custom_notification_activity);

        AddNewCustomNotificationFragment fragment;

        if (savedInstanceState == null) {
            fragment = new AddNewCustomNotificationFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.notification_container, fragment).commit();
        }
    }
    public void addNotification(View view) {
        AddNewCustomNotificationFragment addNotificationFragment = (AddNewCustomNotificationFragment)getSupportFragmentManager().findFragmentById(R.id.notification_container);
        addNotificationFragment.addNotification();
    }
}
