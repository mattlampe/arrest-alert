package com.starnia.arrestalert;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

/**
 * Created by Matt on 12/16/13.
 */
public class SplashActivity extends Activity {

    private static final int SPLASH_TIME_OUT = 4000;
    private static final int IMAGE_SIZE = 500;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onResume() {
        super.onResume();
        Configuration config = getResources().getConfiguration();
        setImageSize(config.screenHeightDp);
    }

    public void setImageSize(int height) {
        ImageView image = (ImageView)findViewById(R.id.imgLogo);
        android.view.ViewGroup.LayoutParams layoutParams = image.getLayoutParams();

        height = height > IMAGE_SIZE ? IMAGE_SIZE : height;

        layoutParams.width = height;
        layoutParams.height = height;
        image.setLayoutParams(layoutParams);
        image.requestLayout();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setImageSize(newConfig.screenHeightDp);
    }
}
