package com.starnia.arrestalert;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.facebook.Session;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LocationSettingsActivity extends Activity {

	LocationSettingsFragment locationSettingsFragment;
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	    getFragmentManager().beginTransaction().replace(android.R.id.content, new LocationSettingsFragment()).commit(); 
	}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }
}