package com.starnia.arrestalert;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Arrest implements Cloneable {
	
	private String name, charges, id, book_date_formatted, mugshot, book_date, more_info_url;
	private Map <String, String> details = new HashMap<String, String>();
	
	public Arrest(String name, String charges, String id, String book_date_formatted, 
			String mugshot, String book_date, String more_info_url, Map<String, String> details) {
		this.setName(name);
		this.setCharges(charges);
		this.setId(id);
		this.setBookDateFormatted(book_date_formatted);
		this.setMugshot(mugshot);
		this.setBook_date(book_date);
		this.setMoreInfoURL(more_info_url);
		this.setDetails(details);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();	
		sb.append(this.getName() + " " + this.getBookDateFormatted());
		return sb.toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCharges() {
		return charges;
	}

	public void setCharges(String charges) {
		this.charges = charges;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBookDateFormatted() {
		return book_date_formatted;
	}

	public void setBookDateFormatted(String book_date_formatted) {
		this.book_date_formatted = book_date_formatted;
	}

	public String getMugshot() {
		return mugshot;
	}

	public void setMugshot(String mugshot) {
		this.mugshot = mugshot;
	}

	public String getBook_date() {
		return book_date;
	}

	public void setBook_date(String book_date) {
		this.book_date = book_date;
	}

	public String getMoreInfoURL() {
		return more_info_url;
	}

	public void setMoreInfoURL(String more_info_url) {
		this.more_info_url = more_info_url;
	}

	public Map <String, String> getDetails() {
		return details;
	}

	public void setDetails(Map <String, String> details) {
		this.details = details;
	}

    @Override
    protected Object clone() {
        Arrest newArrest = null;
        try {
            newArrest = (Arrest)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            Log.e("CLONE", e.toString());
        }

        /*
        private String name, charges, id, book_date_formatted, mugshot, book_date, more_info_url;
	    private Map <String, String> details = new HashMap<String, String>();
         */
        newArrest.name = new String(name);
        newArrest.charges = new String(charges);
        newArrest.id = new String(id);
        newArrest.book_date_formatted = new String(book_date_formatted);
        newArrest.mugshot = new String(mugshot);
        newArrest.book_date = new String(book_date);
        newArrest.more_info_url = new String(more_info_url);
        newArrest.details = new HashMap(details);
        return newArrest;
    }

    public String getDetailsString() {
        Set keys = details.keySet();
        String arrestDetails = "[";
        Iterator keyIterator = keys.iterator();
        while (keyIterator.hasNext()) {
            String key = (String)keyIterator.next();
            arrestDetails += "[\"" + key + "\",\"" + details.get(key) + "\"]";
            if (keyIterator.hasNext())
                arrestDetails += ",";
        }
        arrestDetails += "]";
        return arrestDetails;
    }
}
