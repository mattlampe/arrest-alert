package com.starnia.arrestalert;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class ArrestActivity extends FragmentActivity {

    private static final int IMAGE_HEIGHT = 500;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.arrest_view_activity);
		
		ArrestFragment fragment;
		
		if (savedInstanceState == null) {
            fragment = new ArrestFragment();
            fragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().replace(R.id.arrest_container, fragment).commit();
		}
	}

    @Override
    public void onResume() {
        super.onResume();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.arrest_container);
        setImageSize(fragment);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.arrest_container);
        setImageSize(fragment);
    }

    public void setImageSize(Fragment fragment) {
        Configuration config = getResources().getConfiguration();
        int height = config.screenHeightDp;

        ImageView image = (ImageView)fragment.getView().findViewById(R.id.arrest_image);
        android.view.ViewGroup.LayoutParams layoutParams = image.getLayoutParams();

        if (height > IMAGE_HEIGHT) {
            height = IMAGE_HEIGHT;
        }

        int width = (int)(0.8 * height);

        layoutParams.width = width;
        layoutParams.height = height;
        image.setLayoutParams(layoutParams);
        image.requestLayout();
    }
}
