package com.starnia.arrestalert;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Matt on 1/2/14.
 */
abstract class SearchPoolManager {

    private Collection<ArrestListResponseHandler> handlers;
    private int searches;
    private Context context;

    public SearchPoolManager(Context context, int searches) {
        handlers = new ArrayList<ArrestListResponseHandler>();
        this.searches = searches;
        this.context = context;
    }

    public void addSearchHandler(ArrestListResponseHandler responseHandler) {
        handlers.add(responseHandler);
    }

    public void markComplete(ArrestListResponseHandler responseHandler) {
        handlers.remove(responseHandler);
        searches--;
        if (handlers.isEmpty() && searches == 0)
            handleCallback();
    }

    public void cancelRequests() {
        for (ArrestListResponseHandler handler : handlers) {
            handler.client.cancelRequests(context, true);
        }
    }

    public abstract void handleCallback();
}
