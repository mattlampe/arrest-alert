package com.starnia.arrestalert;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Matt on 12/19/13.
 */
public class CustomAlertFragment extends ListFragment {

    private ArrayAdapter<CustomNotification> arrayAdapter;
    private ActionMode mActionMode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.custom_alert_list_view, container, false);

        DatabaseNotificationHandler db = new DatabaseNotificationHandler(getActivity());
        ArrayList<CustomNotification> notificationList = db.getCustomAlerts();

        arrayAdapter = new ArrayAdapter<CustomNotification>(inflater.getContext(), android.R.layout.simple_list_item_activated_1, notificationList);
        setListAdapter(arrayAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateAlerts();
    }

    private void updateAlerts() {
        DatabaseNotificationHandler db = new DatabaseNotificationHandler(getActivity());
        ArrayList<CustomNotification> notificationList = db.getCustomAlerts();
        ArrayAdapter adapter = (ArrayAdapter)getListAdapter();
        adapter.clear();
        adapter.addAll(notificationList);
        adapter.notifyDataSetChanged();
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.custom_alert_context_menu, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_delete:
                    DatabaseNotificationHandler db = new DatabaseNotificationHandler(getActivity());
                    ArrayList<CustomNotification> customNotifications = db.getCustomAlerts();
                    ListView lv = getListView();
                    SparseBooleanArray checkedItemPositions = lv.getCheckedItemPositions();

                    // delete the items from the list
                    ArrayList<CustomNotification> tempArray = new ArrayList<CustomNotification>();
                    for (int i=0; i<customNotifications.size(); i++) {
                        if (checkedItemPositions.get(i) == true) {
                            CustomNotification notification = customNotifications.get(i);
                            db.deleteCustomNotification(notification.getId());
                            tempArray.add(arrayAdapter.getItem(i));
                        }
                    }

                    for (CustomNotification notification : tempArray) {
                        arrayAdapter.remove(notification);
                    }
                    arrayAdapter.notifyDataSetChanged();

                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return true;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    @Override
    public void onListItemClick(ListView lv, View v, int position, long id) {
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedState) {
        super.onActivityCreated(savedState);

        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                // return if context menu is already created
                if (mActionMode != null) {
                    return false;
                }

                getListView().setItemChecked(position, true);

                // Start the CAB using the ActionMode.Callback defined above
                mActionMode = getActivity().startActionMode(mActionModeCallback);
                return true;
            }
        });
    }
}
