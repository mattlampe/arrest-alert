package com.starnia.arrestalert;

import java.io.IOException;
import java.util.ArrayList;

import java.util.Collection;
import java.util.Collections;

import com.facebook.Session;
import com.facebook.SessionState;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class LocationSettingsFragment extends PreferenceFragment {

    private static ProgressDialog mDialog;
    private ArrestLocation location;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDialog = new ProgressDialog(getActivity(), ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setCancelable(false);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String source_id = prefs.getString("source_id", null);

        if (source_id != null)
            location = MyApp.getLocationById(source_id);

        addPreferencesFromResource(R.xml.location_preferences);

        ListPreference prefListStates = (ListPreference) findPreference("button_location_state_key");
        ListPreference prefListCounties = (ListPreference) findPreference("button_location_county_key");
        CheckBoxPreference prefService = (CheckBoxPreference) findPreference("background_service");
        Preference customPreference = findPreference("custom_alerts");
        prefListStates.setEnabled(false);
        prefListCounties.setEnabled(false);
        customPreference.setEnabled(false);
        prefService.setEnabled(false);
        Preference doneButton = (Preference)findPreference("done_button");
        doneButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                getActivity().finish();
                return false;
            }
        });

        prefListStates.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference arg0, Object arg1) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                ListPreference prefListCounties = (ListPreference) findPreference("button_location_county_key");
                arg0.setIcon(R.drawable.btn_check_buttonless_on);
                String source_id = prefs.getString("source_id", null);
                // if the string has changed update the preference summary and counties
                if (!((String)arg1).equalsIgnoreCase(getStateById(source_id))) {
                    arg0.setSummary((String)arg1);
                    prefListCounties.setEnabled(true);
                    loadCounties((String)arg1);
                    prefListCounties.setSummary("");
                    prefListCounties.setIcon(android.R.drawable.arrow_down_float);
                }
                return true;
            }
        });

        prefListCounties.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference arg0, Object arg1) {
                arg0.setIcon(R.drawable.btn_check_buttonless_on);
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                String oldSourceId = prefs.getString("source_id", null);
                SharedPreferences.Editor editor = prefs.edit();
                String source_id = getSourceId((String)arg1);
                if (oldSourceId == null || !oldSourceId.equalsIgnoreCase(source_id)) {
                    MyApp.setSettingsChanged(true);
                    editor.putString("source_id", source_id);
                    editor.commit();
                }
                arg0.setSummary((String)arg1);
                Preference customPreference = findPreference("custom_alerts");
                customPreference.setEnabled(true);
                return true;
            }
        });

        prefService.setOnPreferenceChangeListener(new FaceBookChangeListener(this));

        customPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent customAlertIntent = new Intent(getActivity(), CustomAlertActivity.class);
                startActivity(customAlertIntent);
                return true;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	// this is the facebook background service
        CheckBoxPreference servicePref =  (CheckBoxPreference) findPreference("background_service");
        ListPreference prefListStates = (ListPreference) findPreference("button_location_state_key");
        ListPreference prefListCounties = (ListPreference) findPreference("button_location_county_key");
        Preference customPreference = findPreference("custom_alerts");
        if (location != null) {
            loadStates();
            loadCounties(location.getState());
            prefListStates.setSummary(location.getState());
            prefListStates.setValue(location.getState());
            prefListStates.setIcon(R.drawable.btn_check_buttonless_on);
            prefListCounties.setEnabled(true);
            prefListCounties.setIcon(R.drawable.btn_check_buttonless_on);
            prefListCounties.setValue(location.getCounty());
            prefListCounties.setSummary((location.getCounty()));
            servicePref.setEnabled(true);
            customPreference.setEnabled(true);
        } else {
            prefListStates.setIcon(android.R.drawable.arrow_down_float);
            prefListCounties.setIcon(android.R.drawable.arrow_down_float);
            servicePref.setEnabled(false);
            servicePref.setChecked(false);
            populateLocationsLists();
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void populateLocationsLists() {
        mDialog.setMessage("Retrieving locations...");
        mDialog.show();

        final ArrayList<ArrestLocation> locations = new ArrayList<ArrestLocation>();
        LocationHandler locationHandler = new LocationHandler(locations) {
            @Override
            public void load() {
                // only update the UI when coming back from an async call if getActivity is not null
                if (getActivity() != null) {
                    ListPreference prefListStates = (ListPreference) findPreference("button_location_state_key");
                    ListPreference prefListCounties = (ListPreference) findPreference("button_location_county_key");
                    CheckBoxPreference prefService = (CheckBoxPreference) findPreference("background_service");
                    Preference customPreference = findPreference("custom_alerts");

                    MyApp.setLocations(locations);

                    loadStates();
                    prefListStates.setEnabled(true);

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    String source_id = prefs.getString("source_id", null);

                    if (source_id != null)
                        location = MyApp.getLocationById(source_id);

                    // after retrieving the locations set the state and county preference if a source_id
                    // is present
                    if (location != null) {
                        loadCounties(location.getState());
                        prefListStates.setSummary(location.getState());
                        prefListStates.setValue(location.getState());
                        prefListStates.setIcon(R.drawable.btn_check_buttonless_on);
                        prefListCounties.setEnabled(true);
                        prefListCounties.setIcon(R.drawable.btn_check_buttonless_on);
                        prefListCounties.setValue(location.getCounty());
                        prefListCounties.setSummary((location.getCounty()));
                        customPreference.setEnabled(true);
                        prefService.setEnabled(true);
                    }
                }

                mDialog.dismiss();
            }

            @Override
            public void handleFailure(Throwable e) {
                // clear the loading dialog
                if (mDialog != null && mDialog.isShowing())
                    mDialog.dismiss();

                StringBuilder sb = new StringBuilder();
                //sb.append("Exception: " + e.toString() + "\n");
                if (e instanceof IOException)
                    sb.append("Network error!  Please check connection and try again.");
                else
                    sb.append("Error retrieving data - " + e.toString());

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                dialogBuilder.setTitle("Could not connect to JailBase API");
                dialogBuilder.setMessage(sb.toString());
                dialogBuilder.setCancelable(false);

                dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog errorDialog = dialogBuilder.create();
                errorDialog.show();
            }
        };
        locationHandler.getLocations();
    }

    public void onResume() {
        super.onResume();
    }

    public void loadCounties(String state) {
        ListPreference prefListCounties = (ListPreference) findPreference("button_location_county_key");
        ArrayList<String> counties = getLocationCounties(state);
        prefListCounties.setEntries(counties.toArray(new String[counties.size()]));
        prefListCounties.setEntryValues(counties.toArray(new String[counties.size()]));
        prefListCounties.setEnabled(true);
    }

    public void loadStates() {
        ListPreference prefListStates = (ListPreference)findPreference("button_location_state_key");
        ArrayList<String> states = getLocationStates();
        prefListStates.setEntries(states.toArray(new String[states.size()]));
        prefListStates.setEntryValues(states.toArray(new String[states.size()]));
        prefListStates.setEnabled(true);
    }

    public ArrayList<String> getLocationStates() {
        ArrayList<String> states = new ArrayList<String>();
        Collection<ArrestLocation> newLocations = MyApp.getLocations();
        for (ArrestLocation tempLocation : newLocations) {
            if (!states.contains(tempLocation.getState()))
                states.add(tempLocation.getState());
        }
        Collections.sort(states);
        return states;
    }

    public ArrayList<String> getLocationCounties(String state) {
        ArrayList<String> counties = new ArrayList<String>();

        Collection<ArrestLocation> newLocations = MyApp.getLocations();
        for (ArrestLocation location : newLocations) {
            if (location.getState().equalsIgnoreCase(state) && !counties.contains(location.getCounty()))
                counties.add(location.getCounty());
        }
        return counties;
    }

    public String getSourceId(String county) {
        String source_id = null;
        Collection<ArrestLocation> newLocations = MyApp.getLocations();
        for (ArrestLocation location : newLocations) {
            if (location.getCounty().equalsIgnoreCase(county))
                source_id = location.getSourceId();
        }
        return source_id;
    }

    private String getStateById(String source_id) {
        String state = null;
        if (MyApp.getLocationsMap().containsKey(source_id)) {

            ArrestLocation location = MyApp.getLocationById(source_id);

            if (location.getSourceId().compareToIgnoreCase(source_id) == 0) {
                state = location.getState();
            }
        }
        return state;
    }

    class FaceBookChangeListener implements OnPreferenceChangeListener {

        private PreferenceFragment fragment;

        FaceBookChangeListener(LocationSettingsFragment fragment) {
            this.fragment = (PreferenceFragment)fragment;
        }
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            // if turning on fb integration get a session and make sure it opens
            if ((Boolean)newValue) {
                MyApp.setSettingsChanged(true);
                Session session = Session.getActiveSession();
                if (session != null && !session.isOpened() && !session.isClosed()) {
                    session.openForRead(new Session.OpenRequest(getActivity()));
                }
                if (session == null || session.isClosed()) {
                    session = Session.openActiveSessionFromCache(getActivity());
                    if (session != null)
                        session.addCallback(new FBCallback());
                }
                if (session == null) {
                    session = Session.openActiveSession(getActivity(), true, new FBCallback());
                }
            } else {
                // if the fb integration is turned off close the session
                Session session = Session.getActiveSession();
                if (session != null) {
                    session.closeAndClearTokenInformation();
                }
            }
            return true;
        }
    }
    class FBCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            /*
            CheckBoxPreference fbPref = (CheckBoxPreference) findPreference("background_service");
            if (state == SessionState.CLOSED_LOGIN_FAILED || state == SessionState.CLOSED) {
                fbPref.setChecked(false);
            }
            else if (state == SessionState.OPENED) {
                fbPref.setChecked(true);
            }
            */
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
    }
}
