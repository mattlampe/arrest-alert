package com.starnia.arrestalert;

import android.content.Context;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class ArrestListResponseHandler extends ArrestFragmentHandler {

    public static final int SEARCH = 0;
    public static final int RECENT = 1;

    public static final int CONNECTION_ERROR = 100;
    public static final int TIMEOUT_ERROR = 101;

	private  ArrayList<Arrest> arrestList;
	private int next_page, start_page;
    private String source_id;
    private int ACTION;
    private String firstName, lastName;

	public ArrestListResponseHandler(Context context, ArrayList<Arrest> arrestList, String source_id, int ACTION) {
        super(context, source_id);
        this.source_id = source_id;
		this.arrestList = arrestList;
        this.ACTION = ACTION;
		setNext_page(start_page);
        next_page = 0;
    }

    public ArrestListResponseHandler(Context context, ArrayList<Arrest> arrestList, String source_id, String firstName, String lastName, int ACTION) {
        super(context, source_id);
        this.source_id = source_id;
        this.arrestList = arrestList;
        this.ACTION = ACTION;
        this.lastName = lastName;
        this.firstName = firstName;
        setNext_page(start_page);
    }
	
	public void onSuccess(JSONObject response) {
		JSONArray arrestsArray = null;
		try {
			if (response.getInt("status") == 1) {
				
				arrestsArray = response.getJSONArray("records");
				next_page = response.getInt("next_page");

				for (int count = 0; count < arrestsArray
						.length(); count++) {
					
					JSONObject JSONarrest = arrestsArray.getJSONObject(count);
					
					String name = JSONarrest.getString("name");
					
					String charges = JSONarrest.getString("charges");
					
					String id = JSONarrest.getString("id");
					
					String book_date_formatted = JSONarrest.getString("book_date_formatted");
					
					JSONArray detailsArray = JSONarrest.getJSONArray("details");
					
					Map<String, String> details = new HashMap<String, String>();
					
					//"details":[["Gender","MALE"],["Race","HISPANIC"],["Age (at arrest)",36],["Height","6'01"],["Weight","200"],["Eyes","BROWN"],["Hair","BLACK"],["Ref #","#P983773"]]
					for (int i=0; i<detailsArray.length(); i++)
					{
						JSONArray tempArrestArray = detailsArray.getJSONArray(i);
						
						details.put(tempArrestArray.getString(0), tempArrestArray.getString(1));
					}
			
					String mugshot = JSONarrest.getString("mugshot");
					
					String book_date = JSONarrest.getString("book_date");
					
					String more_info_url = JSONarrest.getString("more_info_url");
					
					Arrest arrest = new Arrest(name, charges, id, book_date_formatted, 
							mugshot, book_date, more_info_url, details);
					
					arrestList.add(arrest);
				}
                if (next_page > getStart_page())
                    switch (ACTION) {
                        case SEARCH:
                            getSearchArrests(next_page, lastName, firstName);
                            break;
                        case RECENT:
                            getRecentArrests(next_page);
                            break;
                        default:
                            break;
                    }
                else {
                    switch (ACTION) {
                        case SEARCH:
                            MyApp.addMatchedArrests(arrestList);
                            break;
                        case RECENT:
                            MyApp.setArrestList(arrestList);
                            break;
                        default:
                            break;
                    }
                    load();
                }

			} else {
                //TODO: add catch for error from jailbase...i.e. source_id is invalid
            }
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
	}

    @Override
    public void onFailure(Throwable error) {
        handleFailure(error);
    }

    @Override
	public void onFinish() {
		super.onFinish();
	}

	public int getStart_page() {
		return start_page;
	}

	public void setNext_page(int next_page) {
		next_page = next_page;
	}
}