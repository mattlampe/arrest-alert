package com.starnia.arrestalert;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphObject;
import com.loopj.android.http.AsyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;

public class RecentArrestsFragment extends Fragment {

	HashMap<String, ArrestLocation> locations;
	static ArrayList<Arrest> arrestList;
	private GridView arrestListView;
	private ImageAdapter arrayAdapter;
	String[] mugshots, descriptions;
	static ArrestListResponseHandler arrestListResponseHandler;
	static ProgressDialog mDialog;

    static ArrayList<String> fbFriendNames;
    static ArrayList<Arrest> arrestedFriends;

    int scrollIndex = 0;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);  // lets us set the actionbar items programmatically from the fragment
        arrestedFriends = new ArrayList<Arrest>();
		mugshots = new String[] {};  // mugshot url for each arrest
		descriptions = new String[] {};  // description for each arrest
		arrayAdapter = new ImageAdapter(getActivity().getApplicationContext(), mugshots, descriptions);  // lazy adapter for mugshot list
        locations = new HashMap<String, ArrestLocation>();

        mDialog = new ProgressDialog(getActivity(), ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setCancelable(false);

        arrestList = new ArrayList<Arrest>();
        fbFriendNames = new ArrayList<String>();
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

        View view;

        view = inflater.inflate(R.layout.recent_arrests_list_view,
                    container, false);
        if (view != null) {
            arrestListView = (GridView) view.findViewById(R.id.arrest_list);
            arrestListView.setAdapter(arrayAdapter);
            if (savedInstanceState != null) {
                scrollIndex = savedInstanceState.getInt("position", 0);
                arrestListView.setSelection(scrollIndex);
            }
        }
        return view;
	}

    @Override
    public void onSaveInstanceState(Bundle outState) {
        scrollIndex = arrestListView.getFirstVisiblePosition();
        outState.putInt("position", scrollIndex);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
	public void onPause() {
		super.onPause();
        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
	}

    @Override
	public void onResume() {
		super.onResume();

        arrestListView.setSelection(scrollIndex);

        if (arrestListView.getAdapter().isEmpty())
            MyApp.setLoaded(false);

        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getActivity());

        String source_id = prefs.getString("source_id", null);

        if (source_id != null && (MyApp.isSettingsChanged() || !MyApp.isLoaded())) {
            if (MyApp.isSettingsChanged() || !MyApp.isLoaded()) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("source_id", source_id);
                editor.commit();
                MyApp.setSettingsChanged(false);
                getRecentArrests(1);
		    } else {
                loadRecentArrests();
            }
        }
	}

    /*
     *  Retrieve the mugshots and descriptions from JailBase.
     *
     *  This method is called recursively from the ArrestListResponseHandler until all the pages
     *  have been retrieved.
     */
	public void getRecentArrests(final int start_page) {

        if (!mDialog.isShowing()) {
            mDialog.setMessage("Retrieving arrests...");
            mDialog.show();
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String source_id = prefs.getString("source_id", null);

        if (source_id == null) {
            Toast.makeText(getActivity(), "Could not acquire location source_id!", Toast.LENGTH_LONG).show();
            return;
        }

		if (start_page == 1)
            arrestList.clear();
            MyApp.clearArrestList();
		    arrestListResponseHandler = new ArrestListResponseHandler(getActivity().getApplicationContext(),
                    arrestList, source_id, ArrestListResponseHandler.RECENT) {
                @Override
                public void load() {
                // sometimes by the time the result comes back the activity has been destroyed...only update if not null
                if (getActivity() != null)
                    loadRecentArrests();
                }

                @Override
                public void handleFailure(Throwable e) {
                    StringBuilder sb = new StringBuilder();
                    //sb.append("Exception: " + e.toString() + "\n");
                    if (e instanceof IOException)
                        sb.append("Network error!  Please check connection and try again.");
                    else
                        sb.append("Error retrieving data - " + e.getMessage());

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    dialogBuilder.setTitle("Could not connect to JailBase API");
                    dialogBuilder.setMessage(sb.toString());
                    dialogBuilder.setCancelable(false);

                    dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (mDialog != null && mDialog.isShowing())
                                mDialog.dismiss();
                        }
                    });

                    AlertDialog errorDialog = dialogBuilder.create();
                    errorDialog.show();
                }
        };
        arrestListResponseHandler.getRecentArrests(1);
	}

    public void loadRecentArrests() {
        ArrayList<Arrest> arrests = MyApp.getArrestList();
        mugshots = new String[arrests.size()];
        descriptions = new String[arrests.size()];
        for (int i = 0; i < mugshots.length; i++) {
            mugshots[i] = arrests.get(i).getMugshot();
            descriptions[i] = arrests.get(i).getName();
        }

        arrayAdapter = new ImageAdapter(getActivity().getApplicationContext(), mugshots, descriptions);
        arrestListView.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();

        arrestListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent i = new Intent(getActivity(), ArrestActivity.class);
                Arrest arrest = MyApp.getArrestList().get(position);
                String[] details = { arrest.getBook_date(), arrest.getBookDateFormatted(),
                        arrest.getCharges(), arrest.getMugshot(), arrest.getName(), arrest.getMoreInfoURL() };
                i.putExtra("com.example.arrestalert.ArrestDetails", details);
                startActivity(i);
            }
        });

        MyApp.setLoaded(true);
        MyApp.clearMatchedArrests();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean fbIntegration = prefs.getBoolean("background_service", false);

        arrestedFriends.clear();
        if (fbIntegration) {
            getFBFriends();
        } else {
            checkCustomNotifications();
        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        switch(state) {
            case OPENED:
                if (session.isOpened()) {
                    fbFriendNames = new ArrayList<String>();
                    Request request = new Request(session, "me/friends", null, HttpMethod.GET, new Request.Callback() {
                        @Override
                        public void onCompleted(Response response) {

                            GraphObject go = response.getGraphObject();
                            JSONObject jso = go.getInnerJSONObject();
                            JSONArray arr;
                            try {
                                arr = jso.getJSONArray("data");
                                for (int i = 0; i < (arr.length()); i++) {
                                    JSONObject json_obj = arr.getJSONObject(i);
                                    String name = json_obj.getString("name");
                                    fbFriendNames.add(name);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } finally {
                                // populates the matched friends list from FB and jailbase
                                crossCheckFacebook();
                            }
                        }
                    });
                }
        }
    }

    private void crossCheckFacebook() {
        for (int i=0; i<MyApp.getArrestList().size(); i++)
        {
            String name = MyApp.getArrestList().get(i).getName();
            StringTokenizer token =  new StringTokenizer(name);

            String firstName = null;
            if (token.hasMoreTokens())
                firstName = token.nextToken();
            String lastName = null;
            while (token.hasMoreTokens())
                lastName = token.nextToken();

            for (String fbFriend : fbFriendNames)
            {
                fbFriend = fbFriend.toLowerCase(Locale.US);
                if (firstName != null && lastName != null &&
                        fbFriend.contains(firstName.toLowerCase()) &&
                        fbFriend.contains(lastName.toLowerCase()))
                {
                    arrestedFriends.add(MyApp.getArrestList().get(i));
                    break;
                }
            }
        }
        checkCustomNotifications();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession()
                .onActivityResult(getActivity(), requestCode, resultCode, data);
    }

    public void checkCustomNotifications() {
        ArrayList<CustomNotification> customNotifications = getCustomNotifications();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String source_id = prefs.getString("source_id", null);

        if (source_id != null && !customNotifications.isEmpty()) {
            // check the custom notifications to see if any have been arrested
            final SearchPoolManager poolManager = new SearchPoolManager(getActivity().getApplicationContext(), customNotifications.size()) {
                @Override
                public void handleCallback() {
                    if (mDialog != null && mDialog.isShowing())
                        mDialog.dismiss();
                    showAlerts();
                }
            };
            for (CustomNotification cn : customNotifications) {
                String customFirst = cn.getFirstName();
                String customLast = cn.getLastName();
                String customSourceId = cn.getSourceId();
                final ArrayList<Arrest> customNotificationList = new ArrayList<Arrest>();

                if (customSourceId == null)
                	continue;  // could not find source_id for some reason
                
                ArrestListResponseHandler searchHandler = new ArrestListResponseHandler(getActivity().getApplicationContext(),
                        customNotificationList, customSourceId, ArrestListResponseHandler.SEARCH) {
                    @Override
                    public void load() {
                        arrestedFriends.addAll(customNotificationList);
                        poolManager.markComplete(this);
                    }

                    @Override
                    public void handleFailure(Throwable e) {
                        if (getActivity() != null) {
                            poolManager.cancelRequests();

                            Toast.makeText(getActivity(),
                                    "Internet connection error...unable to check notifications.",
                                    Toast.LENGTH_SHORT).show();

                            if (mDialog != null && mDialog.isShowing())
                                mDialog.dismiss();
                        }
                    }
                };
                poolManager.addSearchHandler(searchHandler);
                searchHandler.getSearchArrests(1, customLast, customFirst);
            }
        } else {
            if (mDialog != null && mDialog.isShowing())
                mDialog.dismiss();
        }
    }

    private ArrayList<CustomNotification> getCustomNotifications() {
        DatabaseNotificationHandler db = new DatabaseNotificationHandler(getActivity().getApplicationContext());
        ArrayList<CustomNotification> customNotifications = db.getCustomAlerts();
        return customNotifications;
    }

    private void getFBFriends() {
        try {
            Session session = Session.getActiveSession();
            if (session != null && !session.isOpened() && !session.isClosed()) {
                session.openForRead(new Session.OpenRequest(getActivity()));
            }
            if (session == null || session.isClosed()) {
                session = Session.openActiveSessionFromCache(getActivity());
                if (session != null)
                    session.addCallback(callback);
            }
            if (session == null) {
                session = Session.openActiveSession(getActivity(), true, callback);
            }
            if (session.isOpened()) {
                fbFriendNames = new ArrayList<String>();
                Request request = new Request(session, "me/friends", null, HttpMethod.GET, new Request.Callback() {
                    @Override
                    public void onCompleted(Response response) {

                        GraphObject go = response.getGraphObject();
                        JSONObject jso = go.getInnerJSONObject();
                        JSONArray arr;
                        try {
                            arr = jso.getJSONArray("data");
                            for (int i = 0; i < (arr.length()); i++) {
                                JSONObject json_obj = arr.getJSONObject(i);
                                String name = json_obj.getString("name");
                                fbFriendNames.add(name);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            // populates the matched friends list from FB and jailbase
                            crossCheckFacebook();
                        }
                    }
                });
                RequestAsyncTask task = new RequestAsyncTask(request);
                task.execute();
            }
        } catch (Exception e) {
            Logger.log(e.toString(), Logger.LOG_LEVEL_ERROR);
        }
    }

    public void showAlerts() {
        // if there are matched friends and both FB and custom notifications are done loading alert the user
        if (!arrestedFriends.isEmpty()) {
            DatabaseNotificationHandler dbHandler = new DatabaseNotificationHandler(getActivity().getApplicationContext());
            // get a list of arrests that have already had notifications (so we don't repeat the same notification).
            ArrayList<Arrest> storedArrests = dbHandler.getStoredArrests();

            ArrayList<Arrest> alertFriends = new ArrayList<Arrest>();
            for (int i = 0; i < arrestedFriends.size(); i++) {
                Arrest friendArrest = arrestedFriends.get(i);
                boolean found = false;
                for (int j = 0; j < storedArrests.size(); j++) {
                    if (friendArrest.getName().equalsIgnoreCase(storedArrests.get(j).getName()) &&
                            friendArrest.getBook_date().equalsIgnoreCase(storedArrests.get(j).getBook_date())) {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    alertFriends.add(arrestedFriends.get(i));
            }
            // add ALL the newly found matched friend arrests
            dbHandler.addStoredArrests(arrestedFriends);
            dbHandler.close();

            // add the notification if there are arrests that have not been alerted yet
            if (!alertFriends.isEmpty()) {
                // set the global matched arrests list for viewing if user clicks on notification
                MyApp.setMatchedArrests(alertFriends);

                final ArrayList<Arrest> alerts = alertFriends;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                dialogBuilder.setTitle("Possible arrests found!  Click OK to see who may have been arrested!");
                dialogBuilder.setCancelable(false);
                dialogBuilder.setPositiveButton("View Friends", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MyApp.setMatchedArrests(alerts);
                        Intent matchedArrestIntent = new Intent(getActivity(), MatchedArrestsActivity.class);
                        startActivity(matchedArrestIntent);
                    }
                });
                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                });

                AlertDialog matchedFriendsDialog = dialogBuilder.create();
                matchedFriendsDialog.show();
            }
        }
    }
    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.recent_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.settings_menu:
                startActivity(new Intent(getActivity().getBaseContext(),
                        LocationSettingsActivity.class));
                return true;
            case R.id.refresh_menu:
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                String source_id = prefs.getString("source_id", null);

                if (source_id != null) {
                    getRecentArrests(1);
                }
                return true;
            default:
                return false;
        }
    }
}