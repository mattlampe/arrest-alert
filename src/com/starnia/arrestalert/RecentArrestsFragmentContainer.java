package com.starnia.arrestalert;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Matt on 1/2/14.
 */
public class RecentArrestsFragmentContainer extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.recent_arrests_container, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        FragmentManager fm = getChildFragmentManager();
        if (fm != null) {
            // set the proper fragment based on if there's a source_id selected
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String source_id = prefs.getString("source_id", null);
            Fragment fragment = fm.findFragmentById(R.id.recent_fragment_container);

            if (source_id == null) {
                FragmentTransaction ft = fm.beginTransaction().replace(R.id.recent_fragment_container, new RecentArrestNoLocationFragment(), "search");
                ft.commit();
            } else {
                if (fragment == null || !fragment.getTag().toString().equalsIgnoreCase("recent")) {
                    FragmentTransaction ft = fm.beginTransaction().replace(R.id.recent_fragment_container, new RecentArrestsFragment(), "recent");
                    ft.commit();
                } else
                    fragment.onResume();
            }
        }
    }
}
