package com.starnia.arrestalert;

/**
 * Created by Matt on 12/22/13.
 */
public class CustomNotification {

    private int id;
    private String firstName, lastName, state, county, sourceId;

    public CustomNotification(int id, String firstName, String lastName, String state, String county, String sourceId) {
        this.id = id;
        this.firstName = firstName.trim();
        this.lastName = lastName.trim();
        this.state = state.trim();
        this.county = county.trim();
        this.sourceId = sourceId.trim();
    }

    public int getId() {
        return this.id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getSourceId() {
        return this.sourceId;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Last Name: " + lastName + "\n");
        if (!firstName.isEmpty())
            sb.append("First Name: " + firstName + "\n");
        sb.append("Location: " + county + ", " + state);
        return sb.toString();
    }
}
