package com.starnia.arrestalert;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Matt on 12/22/13.
 */
public class AddNewCustomNotificationFragment extends Fragment {

    Collection<ArrestLocation> locations = MyApp.getLocations();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.add_notification_fragment, container, false);

        Spinner stateSpinner = (Spinner)view.findViewById(R.id.state_spinner);
        Spinner countySpinner = (Spinner)view.findViewById(R.id.county_spinner);

        fillSpinners(view);
        Button cancelButton = (Button)view.findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 getActivity().finish();
            }
        });

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String state = (String)parent.getItemAtPosition(position);

                if (state != null) {
                    Spinner countySpinner = (Spinner)getView().findViewById(R.id.county_spinner);
                    ArrayAdapter adapter = (ArrayAdapter)countySpinner.getAdapter();
                    adapter.clear();
                    adapter.addAll(getLocationCounties(state));
                    countySpinner.setEnabled(true);
                    countySpinner.setClickable(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    public void addNotification() {
        DatabaseNotificationHandler db = new DatabaseNotificationHandler(getActivity());
        EditText lastName = (EditText)getView().findViewById(R.id.last_name);
        EditText firstName = (EditText)getView().findViewById(R.id.first_name);

        Spinner countySpinner = (Spinner)getView().findViewById(R.id.county_spinner);
        Spinner stateSpinner = (Spinner)getView().findViewById(R.id.state_spinner);

        InputMethodManager in = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(lastName.getWindowToken(), 0);
        in.hideSoftInputFromWindow(firstName.getWindowToken(), 0);

        String last = lastName.getText().toString();
        String first = firstName.getText().toString();

        if (stateSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(getActivity(), "Please choose a State", Toast.LENGTH_SHORT).show();
        } else if (last.trim().length() == 0)
            Toast.makeText(getActivity(), "Last name is required", Toast.LENGTH_SHORT).show();
        else {
            String county = (String)countySpinner.getSelectedItem();
            String state = (String)stateSpinner.getSelectedItem();
            String sourceId = getSourceId(county);

            db.addCustomNotification(first, last, state, county, sourceId);
            MyApp.setSettingsChanged(true);
            Toast.makeText(getActivity(), "Custom Notification Added", Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }
    }

    public String getSourceId(String county) {
        String source_id = null;
        for (ArrestLocation location : locations) {
            if (location.getCounty().equalsIgnoreCase(county)) {
                source_id = location.getSourceId();
                break;
            }
        }
        return source_id;
    }

    public void fillSpinners(View view) {
        Spinner stateSpinner = (Spinner)view.findViewById(R.id.state_spinner);
        Spinner countySpinner = (Spinner)view.findViewById(R.id.county_spinner);

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, getStates());

        ArrayAdapter<String> countyAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, getCounties());

        // Apply the adapters to the spinners
        stateSpinner.setAdapter(stateAdapter);
        countySpinner.setAdapter(countyAdapter);
    }

    public ArrayList<String> getStates() {
        ArrayList<String> states = new ArrayList<String>();

        if (locations != null) {
            for (ArrestLocation location : locations) {
                if (!states.contains(location.getState())) {
                    states.add(location.getState());
                }
            }
        }
        Collections.sort(states);
        states.add(0, "Select State...");
        return states;
    }

    public ArrayList<String> getCounties() {
        ArrayList<String> counties = new ArrayList<String>();

        if (locations != null) {
            for (ArrestLocation location : locations) {
                if (!counties.contains(location.getCounty())) {
                    counties.add(location.getCounty());
                }
            }
        }
        Collections.sort(counties);
        counties.add(0, "Select County...");
        return counties;
    }

    public ArrayList<String> getLocationCounties(String state) {
        ArrayList<String> countiesForState = new ArrayList<String>();
        for (ArrestLocation location : locations) {
            if (location.getState().equalsIgnoreCase(state) && !countiesForState.contains(location.getCounty()))
                countiesForState.add(location.getCounty());
        }
        return countiesForState;
    }
}
