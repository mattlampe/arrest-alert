package com.starnia.arrestalert;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Matt on 9/10/13.
 */
public class SearchFragment extends Fragment {

    ArrayList<ArrestLocation> locations = new ArrayList<ArrestLocation>();
    static ProgressDialog mDialog;
    ArrestListResponseHandler arrestListResponseHandler;
    static AsyncHttpClient client;
    static ArrayList<Arrest> arrestList = new ArrayList<Arrest>();
    private boolean updateArrests = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        client = new AsyncHttpClient(); // used to pull Jailbase api info asynchronously
        mDialog = new ProgressDialog(getActivity(), ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setMessage("Retrieving Locations...");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.search_fragment,
                container, false);

        Button searchButton = (Button)view.findViewById(R.id.search);
        if (searchButton != null) {
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSearchButtonClick();
                }
            });
        }

        Spinner stateSpinner = (Spinner)view.findViewById(R.id.state_spinner);
        Spinner countySpinner = (Spinner)view.findViewById(R.id.county_spinner);

        if (MyApp.getLocations().size() == 0) {
            populateLocationsLists(false);
        } else {
            fillSpinners(view);
        }

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateArrests = true;
                String state = (String)parent.getItemAtPosition(position);

                if (state != null) {
                    Spinner countySpinner = (Spinner)getView().findViewById(R.id.county_spinner);
                    ArrayAdapter adapter = (ArrayAdapter)countySpinner.getAdapter();
                    adapter.clear();
                    adapter.addAll(getLocationCounties(state));
                    countySpinner.setEnabled(true);
                    countySpinner.setClickable(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        countySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateArrests = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        TextView.OnEditorActionListener searchListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    onSearchButtonClick();
                    return true;
                }
                return false;
            }
        };

        EditText lastName = (EditText)view.findViewById(R.id.last_name);
        EditText firstName = (EditText)view.findViewById(R.id.first_name);

        lastName.setOnEditorActionListener(searchListener);
        firstName.setOnEditorActionListener(searchListener);

        return view;
    }

    public void populateLocationsLists(final boolean showMatchedFriends) {
        mDialog.show();

        locations.clear();
        LocationHandler locationHandler = new LocationHandler(locations) {
            @Override
            public void load() {
                MyApp.setLocations(locations);
                fillSpinners(getView());

                if (mDialog != null && mDialog.isShowing())
                    mDialog.dismiss();

                if (showMatchedFriends)
                    showMatchedFriends();
            }

            @Override
            public void handleFailure(Throwable e) {
                // clear the loading dialog
                if (mDialog != null && mDialog.isShowing())
                    mDialog.dismiss();

                StringBuilder sb = new StringBuilder();
                //sb.append("Exception: " + e.toString() + "\n");
                if (e instanceof IOException)
                    sb.append("Network error!  Please check connection and try again.");
                else
                    sb.append("Error retrieving data - " + e.toString());

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                dialogBuilder.setTitle("Could not connect to JailBase API");
                dialogBuilder.setMessage(sb.toString());
                dialogBuilder.setCancelable(false);

                dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog errorDialog = dialogBuilder.create();
                errorDialog.show();
            }
        };
        locationHandler.getLocations();
    }

    public ArrayList<String> getLocationCounties(String state) {
        ArrayList<String> countiesForState = new ArrayList<String>();
        Collection<ArrestLocation> allLocations = MyApp.getLocations();
        for (ArrestLocation location : allLocations) {
            if (location.getState().equalsIgnoreCase(state) && !countiesForState.contains(location.getCounty()))
                countiesForState.add(location.getCounty());
        }
        return countiesForState;
    }

    public void fillSpinners(View view) {
        Spinner stateSpinner = (Spinner)view.findViewById(R.id.state_spinner);
        Spinner countySpinner = (Spinner)view.findViewById(R.id.county_spinner);

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, getStates());

        ArrayAdapter<String> countyAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, getCounties());

        // Apply the adapters to the spinners
        stateSpinner.setAdapter(stateAdapter);
        countySpinner.setAdapter(countyAdapter);
    }

    public ArrayList<String> getStates() {
        ArrayList<String> states = new ArrayList<String>();
        Collection<ArrestLocation> arrestLocations = MyApp.getLocations();
        if (arrestLocations != null) {
            for (ArrestLocation location : arrestLocations) {
                if (!states.contains(location.getState())) {
                    states.add(location.getState());
                }
            }
        }
        Collections.sort(states);
        states.add(0, "Select State...");
        return states;
    }

    public ArrayList<String> getCounties() {
        ArrayList<String> counties = new ArrayList<String>();
        Collection<ArrestLocation> arrestLocations = MyApp.getLocations();
        if (arrestLocations != null) {
            for (ArrestLocation location : arrestLocations) {
                if (!counties.contains(location.getCounty())) {
                    counties.add(location.getCounty());
                }
            }
        }
        Collections.sort(counties);
        counties.add(0, "Select County...");
        return counties;
    }

    public void onSearchButtonClick() {
        EditText lastName = (EditText)getView().findViewById(R.id.last_name);
        EditText firstName = (EditText)getView().findViewById(R.id.first_name);

        Spinner stateSpinner = (Spinner)getView().findViewById(R.id.state_spinner);
        Spinner countySpinner = (Spinner)getView().findViewById(R.id.county_spinner);

        InputMethodManager in = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(lastName.getWindowToken(), 0);
        in.hideSoftInputFromWindow(firstName.getWindowToken(), 0);

        String last = lastName.getText().toString();

        if (stateSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(getActivity(), "Please choose a State", Toast.LENGTH_SHORT).show();
        } else if (last.trim().length() == 0)
            Toast.makeText(getActivity(), "Last name is required", Toast.LENGTH_SHORT).show();
        else {
            String county = (String)countySpinner.getSelectedItem();
            String source_id = getSourceId(county);
            // if the spinners are filled source_id should never be null
            if (source_id == null)
            {
            	Toast.makeText(getActivity(), "An error has occured.  Please try again.", Toast.LENGTH_SHORT).show();
            	populateLocationsLists(false);  // refresh the locations
            }
            else
            {
            	getRecentArrests(1, getSourceId(county));
            }
        }
    }

    public void getRecentArrests(int startPage, String source_id) {
        MyApp.clearMatchedArrests();

        if (!mDialog.isShowing()) {
            mDialog.setMessage("Searching...");
            mDialog.setCancelable(false);
            mDialog.show();
        }

        if (startPage == 1)
            arrestList.clear();
        arrestListResponseHandler = new ArrestListResponseHandler(getActivity().getApplicationContext(),
                arrestList, source_id, ArrestListResponseHandler.SEARCH) {
            @Override
            public void load() {
                updateArrests = false;
                showMatchedFriends();
                mDialog.dismiss();
            }

            @Override
            public void handleFailure(Throwable e) {
                // clear the loading dialog
                if (mDialog != null && mDialog.isShowing())
                    mDialog.dismiss();

                StringBuilder sb = new StringBuilder();
                if (e instanceof IOException)
                    sb.append("Network error!  Please check connection and try again.");
                else
                    sb.append("Error retrieving data - " + e.getMessage());

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                dialogBuilder.setTitle("Could not connect to JailBase API");
                dialogBuilder.setMessage(sb.toString());
                dialogBuilder.setCancelable(false);

                dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog errorDialog = dialogBuilder.create();
                errorDialog.show();
            }
        };
        // get the first and last names from the spinners to search with
        String lastName = "";
        EditText lastNameView = (EditText)getView().findViewById(R.id.last_name);
        if (lastNameView != null)
            lastName = lastNameView.getText().toString().trim();

        String firstName = "";
        EditText firstNameView = (EditText)getView().findViewById(R.id.first_name);
        if (firstNameView != null)
            firstName = firstNameView.getText().toString().trim();

        arrestListResponseHandler.getSearchArrests(1, lastName, firstName);
    }

    public void showMatchedFriends() {
        ArrayList<Arrest> matchedArrests = MyApp.getMatchedArrests();
        if (matchedArrests.size() == 0) {
            Toast.makeText(getActivity(), "No matches found", Toast.LENGTH_LONG).show();
        } else {
            Intent matchedArrestIntent = new Intent(getActivity(), MatchedArrestsActivity.class);
            startActivity(matchedArrestIntent);
        }
    }

    public String getSourceId(String county) {
        String source_id = null;
        for (ArrestLocation location : MyApp.getLocations()) {
            if (location.getCounty().equalsIgnoreCase(county)) {
                source_id = location.getSourceId();
                break;
            }
        }
        return source_id;
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.settings_menu, menu);
    }


}
