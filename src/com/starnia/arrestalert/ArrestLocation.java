package com.starnia.arrestalert;

import android.util.Log;

public class ArrestLocation implements Cloneable, Comparable<ArrestLocation> {
	private String source_id, state, county, name;
    boolean hasMugshots;
	
	public ArrestLocation(String source_id, String state, String county, String name, boolean hasMugshots) {
		this.source_id = source_id;
		this.state = state;
        this.county = county;
		this.name = name;
        this.hasMugshots = hasMugshots;
	}
	
	public String getSourceId() {
		return this.source_id;
	}
	
	public String getState() {
		return this.state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

    public String getCounty() {
        return this.county;
    }

    @Override
    protected Object clone() {
        ArrestLocation newArrestLocation = null;
        try {
            newArrestLocation = (ArrestLocation)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            Log.e("CLONE", e.toString());
        }
        newArrestLocation.source_id = new String(source_id);
        newArrestLocation.state = new String(state);
        newArrestLocation.county = new String(county);
        newArrestLocation.name = new String(name);
        return newArrestLocation;
    }

    @Override
    public int compareTo(ArrestLocation another) {
        return state.compareTo(another.getState());
    }
}
